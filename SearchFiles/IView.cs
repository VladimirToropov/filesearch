﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace SearchFiles
{
    public interface IView
    {
        void Show();
        void Close();
    }

    public interface ISearchView : IView
    {
        event Action StartSearch;
        event Action StopSearch;
        event Action Closing;

        SynchronizationContext Context { get; }
        TreeView Tree { get; }
        ToolStripStatusLabel FileLabel { get; }
        ToolStripStatusLabel TimeLabel { get; }
        Button StartBtn { get; }
        Button StopBtn { get; }
        TextBox Folder { get; }
        TextBox Pattern { get; }
        TextBox SearchText { get; }

        void ShowAlert(string title, string message);
    }

    public interface IPresenter
    {
        void Run();
    }
}