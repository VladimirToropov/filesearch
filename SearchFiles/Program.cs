﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SearchFiles
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var presenter = new SearchController(new Form1(), new SearchService()); //создаем экземпляр контроллера
            presenter.Run();
        }
    }
}
