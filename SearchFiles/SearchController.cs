﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace SearchFiles
{
    public class SearchController : IPresenter
    {
        private const string StartBtnText = "Начать поиск";
        private const string CancelBtnText = "Отменить поиск";

        private const string StopBtnText = "Остановить поиск";
        private const string ContinueBtnText = "Продолжить поиск";

        private const string TotalFilesText = "Обработано файлов: ";
        private const string CurrentFileText = " Текущий файл: ";

        private const int TreeDelayUpdateTime = 30;

        private int _totalFiles;

        private readonly ISearchView _view;
        private readonly ISearchService _service;

        private ConcurrentQueue<string> _searchResultsPaths;
        private CancellationTokenSource _cancelSource;

        /// <summary>
        /// Таймер безопасный для потока
        /// </summary>
        private Timer _timer;
        private Timer _resultsTimer;

        private DateTime _startTime;
        private DateTime _stopTime;

        public SearchController(ISearchView view, ISearchService service) //конструктор
        {
            _view = view;
            _service = service; //передаем CTS и количество обработаных файлов

            // Нажатие кнопки начала
            _view.StartSearch += ViewOnStartSearch;

            // Нажатие кнопки остановки
            _view.StopSearch += ViewOnStopSearch;

            // Сервис поиска нашёл совпадение (добавляем в TreeView)
            _service.OnTreeUpdate += path => { _searchResultsPaths.Enqueue(path); };

            _view.Closing +=
                () =>
                {
                    SettingsManager.Save(new SavedData() //сохраняем данные при закрытии формы
                    {
                        DefaultDirectory = _view.Folder.Text,
                        FilePattern = _view.Pattern.Text,
                        TextToSearch = _view.SearchText.Text
                    });
                };
        }

        private void ViewOnStartSearch()
        {
            if (_view.StartBtn.Text.Equals(CancelBtnText))
            {
                ResetSearch();

                return;
            }
            StartSearch(_view.Folder.Text, _view.Pattern.Text, _view.SearchText.Text);
        }

        private void ViewOnStopSearch()
        {
            if (_view.StopBtn.Text.Equals(ContinueBtnText))
            {
                ContinueSearch(_view.Folder.Text, _view.Pattern.Text, _view.SearchText.Text);
                return;
            }
            StopSearch();
        }

        private void OnStatusUpdate(string currentFile, bool isSearchEnded)
        {
            if (isSearchEnded)
                ResetSearch();

            if (isSearchEnded && _totalFiles == 0)
                _view.ShowAlert("Файлы по шаблону не найдены", "Проверьте шаблон файла. Можно использовать звёздочки (*), знаки вопроса (?) и квадратные скобки ([]).");

            _totalFiles++;

            if (!_cancelSource.IsCancellationRequested)
                UpdateLabels(_totalFiles, currentFile);
        }

        private void StartSearch(string folder, string pattern, string searchText)
        {
            // Обновляем счётчик файлов и текущий файл
            _service.OnStatusUpdate += OnStatusUpdate;

            #region InputCheck

            if (String.IsNullOrWhiteSpace(folder))
            {
                _view.ShowAlert("Выберите папку", "Пожалуйста, выберите папку для поиска файлов");
                return;
            }

            if (String.IsNullOrWhiteSpace(pattern))
            {
                _view.ShowAlert("Заполните шаблон", "Пожалуйста, заполните шаблон для поиска файлов");
                return;
            }

            if (String.IsNullOrWhiteSpace(searchText))
            {
                _view.ShowAlert("Заполните текст для поиска", "Пожалуйста, заполните текст для поиска");
                return;
            }

            #endregion

            SettingsManager.Save(new SavedData { DefaultDirectory = folder, FilePattern = pattern, TextToSearch = searchText }); //сохранеям текущие данные в форме

            // Меняем поведение кнопки поиска
            UpdateStartBtn(CancelBtnText);

            // Настройки для нового поиска
            NewSearch();

            _searchResultsPaths = new ConcurrentQueue<string>();

            _timer = new Timer(100);
            _timer.Elapsed += OnTimerStatusUpdate;
            _timer.Start();

            _startTime = DateTime.Now;

            _resultsTimer = new Timer(TreeDelayUpdateTime);
            _resultsTimer.Elapsed += OnResultsTimerUpdate;
            _resultsTimer.Start();

            // Запуск сервиса поиска
            _cancelSource = new CancellationTokenSource();

            Task.Run(() => //запускаем задачу
            {
                try
                {
                    Task<int> search = _service.StartSearch(folder, pattern, searchText, _cancelSource);
                }
                catch (OperationCanceledException)
                {
                }
                catch (Exception)
                {
                    _view.ShowAlert("Ошибка", "Неизвестная ошибка при поиске");
                }

            });

        }
        private void StopSearch()
        {
            if (_timer != null)
                _timer.Stop();

            UpdateStopBtn(ContinueBtnText);

            _service.OnStatusUpdate -= OnStatusUpdate;
        }

        private void ContinueSearch(string folder, string pattern, string searchText)
        {
            if (_timer != null)
                _timer.Start();

            UpdateStopBtn(StopBtnText);

            _service.OnStatusUpdate += OnStatusUpdate;
        }

        private void NewSearch()
        {
            _view.Tree.Nodes.Clear(); //очищаем TreeView

            UpdateLabels(0, "-");

            _totalFiles = 0;

            if (_timer != null)
            {
                _timer.Stop();
                _timer = null;
            }

            if (_resultsTimer != null)
            {
                _resultsTimer.Stop();
                _resultsTimer = null;
            }
        }

        /// <summary>
        /// Выгрузка результатов поиска в TreeView
        /// </summary>
        private void OnResultsTimerUpdate(object sender, ElapsedEventArgs e)
        {
            if (_searchResultsPaths.Count <= 0) return;

            string path;
            _searchResultsPaths.TryDequeue(out path);

            if (!String.IsNullOrWhiteSpace(path))
                _view.Context.Post(delegate { PopulateTree(_view.Tree, path, '\\'); }, null);
        }

        private void OnTimerStatusUpdate(object sender, ElapsedEventArgs e)
        {
            if (_view.StopBtn.Text.Equals(ContinueBtnText))
                return;
            _stopTime = DateTime.Now;
            TimeSpan elapsed = _stopTime - _startTime;

            _view.Context.Post(delegate
            {
                _view.TimeLabel.Text = elapsed.ToString(@"hh\:mm\:ss");

            }, null);
        }

        private void UpdateStartBtn(string text)
        {
            if (String.IsNullOrWhiteSpace(text))
                throw new Exception("Пустой текст на кнопке");

            _view.Context.Post(delegate
            {
                _view.StartBtn.Text = text;
                _view.StartBtn.Refresh();

            }, null);
        }

        private void UpdateStopBtn(string text)
        {
            if (String.IsNullOrWhiteSpace(text))
                throw new Exception("Пустой текст на кнопке");

            _view.Context.Post(delegate
            {
                _view.StopBtn.Text = text;
                _view.StopBtn.Refresh();

            }, null);
        }

        private void UpdateLabels(int totalFiles, string currentFile)
        {
            _view.Context.Post(delegate
            {
                _view.FileLabel.Text = TotalFilesText + totalFiles + CurrentFileText + currentFile;

            }, null);
        }

        private void ResetSearch()
        {
            if (_cancelSource != null)
                _service.CancelSearch();

            _service.OnStatusUpdate -= OnStatusUpdate;

            UpdateStartBtn(StartBtnText);

            if (_timer != null)
            {
                _timer.Stop();
                _timer = null;
            }
        }

        private void PopulateTree(TreeView treeView, string path, char pathSeparator) //заполняем дерево в TreeView
        {
            TreeNode lastNode = null;

            string subPathAgg = string.Empty;
            foreach (string subPath in path.Split(pathSeparator)) //разделяем на подпапки
            {
                subPathAgg += subPath + pathSeparator;
                TreeNode[] nodes = treeView.Nodes.Find(subPathAgg, true);

                if (nodes.Length == 0)
                    if (lastNode == null)
                        lastNode = treeView.Nodes.Add(subPathAgg, subPath, 0);

                    else
                        lastNode = lastNode.Nodes.Add(subPathAgg, subPath, 0);
                else
                    lastNode = nodes[0];
            }
        }

        public void OnExit(object sender, EventArgs e) //метод срабатывает при выходе из приложения, сохраняет все данные из формы в файл
        {
            SettingsManager.Save(new SavedData
            {
                DefaultDirectory = _view.Folder.Text,
                FilePattern = _view.SearchText.Text,
                TextToSearch = _view.SearchText.Text
            });
        }

        #region RunLogic
        public void Run() //метод, запускаемый из Program на экземпляре SearchController'а 
        {
            var settings = SettingsManager.Load(); //загружаем ранее сохраненные настройки приложения

            _view.Context.Post(delegate
            {
                _view.Folder.Text = settings.DefaultDirectory;
                _view.Folder.Refresh();

                _view.Pattern.Text = settings.FilePattern;
                _view.Pattern.Refresh();

                _view.SearchText.Text = settings.TextToSearch;
                _view.SearchText.Refresh();

            }, null);

            _view.Show();
        }
        #endregion
    }
}
