﻿using System;
using System.IO;
using System.Linq.Expressions;
using System.Windows.Forms;
using YamlDotNet.Serialization;

namespace SearchFiles
{
    public class SavedData
    {
        public string DefaultDirectory { get; set; }
        public string FilePattern { get; set; }
        public string TextToSearch { get; set; }
    }

    public static class SettingsManager //класс настроек для приложения
    {
        private const string SettingsFile = "settings.ini";

        public static void Save(SavedData saveData) //вызывается при закрытии приложения, сериализует данные, находящиеся на форме в файл SettingsFile
        {
            var serializer = new Serializer(); 

            using (TextWriter writer = File.CreateText(Path.Combine(Application.StartupPath, SettingsFile)))
            {
                serializer.Serialize(writer, saveData);
            }
        }

        public static SavedData Load() //вызывается при открытии приложения
        {
            try
            {
                using (TextReader reader = File.OpenText(Path.Combine(Application.StartupPath, SettingsFile)))
                {
                    var deserializer = new Deserializer(); //десериализуем файл настроек, сохраненный при закрытии 
                    return deserializer.Deserialize<SavedData>(reader);
                }
            }
            catch (FileNotFoundException)
            {
                string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                return new SavedData { DefaultDirectory = folderPath, FilePattern = "*.txt", TextToSearch = "a" }; //если файл не найден - подставляем значения по умолчанию
            }
        }
    }
}
