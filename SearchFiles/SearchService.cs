﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SearchFiles
{
    public class SearchService : ISearchService
    {
        private CancellationTokenSource _cancelSource;

        public event TreeUpdate OnTreeUpdate;
        public event StatusUpdate OnStatusUpdate;

        private int _proceededFiles;

        public async Task<int> StartSearch(string path, string pattern, string searchText, CancellationTokenSource cancelSource)
        {
            #region InputCheck

            if (String.IsNullOrWhiteSpace(path))
                throw new ArgumentNullException("Не выбрана директория");

            if (String.IsNullOrWhiteSpace(pattern))
                throw new ArgumentNullException("Не выбран шаблон");

            if (String.IsNullOrWhiteSpace(searchText))
                throw new ArgumentNullException("Не выбран текст для поиска");

            #endregion

            // Регистрируем отмену
            _cancelSource = cancelSource;

            // Рекурсивно обходим все папки и проверям файлы
            _proceededFiles = 0;

            await GetDirectoryAsync(new DirectoryInfo(path), pattern, searchText, _cancelSource.Token);

            // Передаём сообщение об окончании поиска
            OnStatusUpdate?.Invoke("Поиск окончен", true);

            return _proceededFiles;
        }

        private async Task GetDirectoryAsync(DirectoryInfo rootFolder, string pattern, string searchText, CancellationToken cancellation)
        {
            FileInfo[] files = null;
            DirectoryInfo[] subDirs;

            try
            {
                files = rootFolder.GetFiles(pattern);
            }
            catch (UnauthorizedAccessException e)
            {
            }
            catch (DirectoryNotFoundException e)
            {
            }

            if (files != null)
            {
                foreach (FileInfo fi in files)
                    if (fi.Directory != null) await ProcessFileAsync(fi.FullName, searchText, cancellation);

                subDirs = rootFolder.GetDirectories();

                foreach (DirectoryInfo dirInfo in subDirs)
                    await GetDirectoryAsync(dirInfo, pattern, searchText, cancellation);
            }
        }

        private async Task ProcessFileAsync(string path, string searchText, CancellationToken cancelToken)
        {
            // Можно отменить поиск при обработке каждого файла
            if (cancelToken.IsCancellationRequested)
                await Task.FromResult(false);

            // Если в файле найдено слово, добавляем информацию о файле в дерево
            string fileName = Path.GetFileName(path);

            string searchString = searchText.Trim();

            if (File.ReadLines(path).Any(line => line.Contains(searchString)) && !cancelToken.IsCancellationRequested)
                ResultFound(path);

            // Обновляем счётчик
            if (OnStatusUpdate != null && !cancelToken.IsCancellationRequested)
                OnStatusUpdate(fileName, false);

            _proceededFiles++;
            await Task.FromResult(true);
        }

        private void ResultFound(string path)
        {
            OnTreeUpdate?.Invoke(path);
        }

        public void CancelSearch()
        {
            if (_cancelSource != null) _cancelSource.Cancel();
        }
    }
}
