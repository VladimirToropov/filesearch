﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SearchFiles
{
    public partial class Form1 : Form, ISearchView
    {
        private readonly SynchronizationContext _sync;

        public Form1()
        {
            _sync = SynchronizationContext.Current;
            InitializeComponent();

            FormClosing += delegate { Closing?.Invoke(); };
        }


        public new void Show()
        {
            Application.Run(this); //запускаем приложение
        }

        public new void Close()
        {
        }

        public SynchronizationContext Context
        {
            get { return _sync; }
        }

        public TreeView Tree
        {
            get { return treeView1; }
        }

        public ToolStripStatusLabel FileLabel
        {
            get { return statusStrip; }
        }
        public ToolStripStatusLabel TimeLabel
        {
            get { return toolStripTime; }
        }

        public Button StartBtn
        {
            get { return startBtn; }
        }
        
        public Button StopBtn 
        {
            get { return stopBtn; }
        }

        public TextBox Folder
        {
            get { return dirInput; }
        }

        public TextBox Pattern
        {
            get { return patternInput; }
        }

        public TextBox SearchText
        {
            get { return searchTextInput; }
        }

        public event Action Closing;
        public event Action StartSearch;
        public event Action StopSearch;

        public void ShowAlert(string title, string message)
        {
            MessageBox.Show(this, message, title);
        }

        private void fileBtn_Click(object sender, EventArgs e) //обработчик нажатия на кнопку Выбрать папку
        {
            FolderBrowserDialog diag = new FolderBrowserDialog();
            diag.ShowDialog();

            string path = diag.SelectedPath;

            if (!String.IsNullOrWhiteSpace(path))
                dirInput.Text = path;
        }

        private void startBtn_Click(object sender, EventArgs e) //обработчик нажатия на кнопку Старт
        {
            StartSearch?.Invoke();
        }

        //мои изменения
        private void stopBtn_Click(object sender, EventArgs e) //обработчик нажатия на кнопку Стоп
        {
            StopSearch?.Invoke();
        }
    }
}
