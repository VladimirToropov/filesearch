﻿using System.Threading;
using System.Threading.Tasks;

namespace SearchFiles
{
    public delegate void TreeUpdate(string path);
    public delegate void StatusUpdate(string currentFile, bool isSearchEnded);

    public interface ISearchService
    {
        event TreeUpdate OnTreeUpdate;
        event StatusUpdate OnStatusUpdate;
        Task<int> StartSearch(string folder, string pattern, string searchText, CancellationTokenSource cancel);
        void CancelSearch();
    }
}
